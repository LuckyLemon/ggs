﻿name="Das Vaterland - Colorized Portraits Submod"
dependencies= {
   "Das Vaterland"
   "Das Vaterland - Dev Build"
}
tags={
	"Graphics"
	"Ideologies"
	"Historical"
	"Utilities"
}
supported_version="1.9.*"
