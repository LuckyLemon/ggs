﻿name="Das Vaterland - Colorized Portraits Submod"
path="mod/DV_Colorized_Portraits/"
dependencies= {
   "Das Vaterland"
}
tags={
	"Graphics"
	"Ideologies"
	"Historical"
	"Utilities"
}
supported_version="1.9.*"
