defined_text = {
	name = GetCoalitionType

	text = {
		trigger = {
			check_variable = { current_lib_icon_frame = 2 }
			check_variable = { current_con_icon_frame = 2 }
			check_variable = { current_socdem_icon_frame = 2 }
		}
		localization_key = COALITION_Democratic
	}

	text = {
		trigger = {
			check_variable = { current_lib_icon_frame = 2 }
			check_variable = { current_socdem_icon_frame = 2 }
			check_variable = { current_soc_icon_frame = 2 }
		}
		localization_key = COALITION_Popular_Front
	}

	text = {
		trigger = {
			check_variable = { current_soc_icon_frame = 2 }
			check_variable = { current_com_icon_frame = 2 }
		}
		localization_key = COALITION_Left_Coalition
	}

	text = {
		trigger = {
			check_variable = { current_natsynd_icon_frame = 2 }
			check_variable = { current_natpop_icon_frame = 2 }
			check_variable = { current_natcorp_icon_frame = 2 }
		}
		localization_key = COALITION_Right_League
	}

	text = {
		trigger = {
			check_variable = { current_despot_icon_frame = 2 }
			check_variable = { current_natpop_icon_frame = 2 }
		}
		localization_key = COALITION_National_Front
	}

	text = {
		trigger = {
			check_variable = { current_despot_icon_frame = 2 }
			check_variable = { current_con_icon_frame = 2 }
		}
		localization_key = COALITION_Conservative_Coalition
	}

	text = {
		trigger = {
			check_variable = { current_lib_icon_frame = 1 }
			check_variable = { current_con_icon_frame = 1 }
			check_variable = { current_socdem_icon_frame = 1 }
			check_variable = { current_soc_icon_frame = 1 }
			check_variable = { current_com_icon_frame = 1 }
			check_variable = { current_natsynd_icon_frame = 1 }
			check_variable = { current_natpop_icon_frame = 1 }
			check_variable = { current_natcorp_icon_frame = 1 }
			check_variable = { current_despot_icon_frame = 1 }
		}
		localization_key = COALITION_None
	}

	text = { localization_key = COALITION_Big_Tent }
}

defined_text = {
	name = GetCoalitionDesc

	text = {
		trigger = {
			check_variable = { current_lib_icon_frame = 2 }
			check_variable = { current_con_icon_frame = 2 }
			check_variable = { current_socdem_icon_frame = 2 }
		}
		localization_key = COALITION_Democratic_Desc
	}

	text = {
		trigger = {
			check_variable = { current_com_icon_frame = 2 }
			check_variable = { current_socdem_icon_frame = 2 }
			check_variable = { current_soc_icon_frame = 2 }
		}
		localization_key = COALITION_Popular_Front_Desc
	}

	text = {
		trigger = {
			check_variable = { current_soc_icon_frame = 2 }
			check_variable = { current_com_icon_frame = 2 }
		}
		localization_key = COALITION_Left_Coalition_Desc
	}

	text = {
		trigger = {
			check_variable = { current_natsynd_icon_frame = 2 }
			check_variable = { current_natpop_icon_frame = 2 }
			check_variable = { current_natcorp_icon_frame = 2 }
		}
		localization_key = COALITION_Right_League_Desc
	}

	text = {
		trigger = {
			check_variable = { current_despot_icon_frame = 2 }
			check_variable = { current_natpop_icon_frame = 2 }
		}
		localization_key = COALITION_National_Front_Desc
	}

	text = {
		trigger = {
			check_variable = { current_despot_icon_frame = 2 }
			check_variable = { current_con_icon_frame = 2 }
		}
		localization_key = COALITION_Conservative_Coalition_Desc
	}

	text = {
		trigger = {
			check_variable = { current_lib_icon_frame = 1 }
			check_variable = { current_con_icon_frame = 1 }
			check_variable = { current_socdem_icon_frame = 1 }
			check_variable = { current_soc_icon_frame = 1 }
			check_variable = { current_com_icon_frame = 1 }
			check_variable = { current_natsynd_icon_frame = 1 }
			check_variable = { current_natpop_icon_frame = 1 }
			check_variable = { current_natcorp_icon_frame = 1 }
			check_variable = { current_despot_icon_frame = 1 }
		}
		localization_key = COALITION_None_Desc
	}

	text = { localization_key = COALITION_Big_Tent_Desc }
}

definedText = {
	name = GetHoGTitle
	##Germany##
	text = {
		trigger = { has_idea_with_trait = POSITION_Reichskanzler }
		localization_key = POSITION_Reichskanzler
	}
	text = {
		trigger = { has_idea_with_trait = POSITION_Parteikanzler }
		localization_key = POSITION_Parteikanzler
	}
	text = {
		trigger = { has_idea_with_trait = POSITION_Prime_Minister }
		localization_key = POSITION_Prime_Minister
	}
}

definedText = {
	name = GetHoGName
	##Germany##
	text = {
		trigger = { has_idea = HOG_Albert_Hugenberg }
		localization_key = HOG_Albert_Hugenberg
	}
	text = {
		trigger = { has_idea = HOG_Konrad_Adenauer }
		localization_key = HOG_Konrad_Adenauer
	}
	text = {
		trigger = { has_idea = HOG_Otto_Wels }
		localization_key = HOG_Otto_Wels
	}
	text = {
		trigger = { has_idea = HOG_Hans_Speidel }
		localization_key = HOG_Hans_Speidel
	}
	text = {
		trigger = { has_idea = HOG_Anton_Drexlar }
		localization_key = HOG_Anton_Drexlar
	}
	text = {
		trigger = { has_idea = HOG_Otto_Strasser }
		localization_key = HOG_Otto_Strasser
	}
	text = {
		trigger = { has_idea = HOG_Ernst_Thalmann }
		localization_key = HOG_Ernst_Thalmann
	}
	text = {
		trigger = { has_idea = HOG_Gustav_Stressemann }
		localization_key = HOG_Gustav_Stressemann
	}
}

definedText = {
	name = GetHoGPic
	text = {
		trigger = { has_idea = HOG_Albert_Hugenberg }
		localization_key = "GFX_P_D_Alfred_Hugenberg"
	}
	text = {
		trigger = { has_idea = HOG_Konrad_Adenauer }
		localization_key = "GFX_P_C_Konrad_Adenauer"
	}
	text = {
		trigger = { has_idea = HOG_Otto_Wels }
		localization_key = "GFX_P_SD_Otto_Wels"
	}
	text = {
		trigger = { has_idea = HOG_Hans_Speidel }
		localization_key = "GFX_P_NC_Hans_Speidel"
	}
	text = {
		trigger = { has_idea = HOG_Anton_Drexlar }
		localization_key = "GFX_P_NP_Anton_Drexlar"
	}
	text = {
		trigger = { has_idea = HOG_Otto_Strasser }
		localization_key = "GFX_P_NS_Otto_Strasser"
	}
	text = {
		trigger = { has_idea = HOG_Ernst_Thalmann }
		localization_key = "GFX_P_V_Ernst_Thalmann"
	}
	text = {
		trigger = { has_idea = HOG_Gustav_Stressemann }
		localization_key = "GFX_P_L_Gustav_Stresemann"
	}
}

definedText = {
	name = GetOppPic
	text = {
		trigger = { original_tag = GER has_idea = OPP_Albert_Hugenberg }
		localization_key = "GFX_P_D_Alfred_Hugenberg"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Konrad_Adenauer }
		localization_key = "GFX_P_C_Konrad_Adenauer"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Otto_Wels }
		localization_key = "GFX_P_SD_Otto_Wels"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Hans_Speidel }
		localization_key = "GFX_P_NC_Hans_Speidel"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Anton_Drexlar }
		localization_key = "GFX_P_NP_Anton_Drexlar"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Otto_Strasser }
		localization_key = "GFX_P_NS_Otto_Strasser"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Ernst_Thalmann }
		localization_key = "GFX_P_V_Ernst_Thalmann"
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Gustav_Stressemann }
		localization_key = "GFX_P_L_Gustav_Stresemann"
	}
}

definedText = {
	name = GetOppName
	##Germany##
	text = {
		trigger = { original_tag = GER has_idea = OPP_Albert_Hugenberg }
		localization_key = HOG_Albert_Hugenberg
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Konrad_Adenauer }
		localization_key = HOG_Konrad_Adenauer
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Otto_Wels }
		localization_key = HOG_Otto_Wels
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Hans_Speidel }
		localization_key = HOG_Hans_Speidel
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Anton_Drexlar }
		localization_key = HOG_Anton_Drexlar
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Otto_Strasser }
		localization_key = HOG_Otto_Strasser
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Ernst_Thalmann }
		localization_key = HOG_Ernst_Thalmann
	}
	text = {
		trigger = { original_tag = GER has_idea = OPP_Gustav_Stressemann }
		localization_key = HOG_Gustav_Stressemann
	}
}