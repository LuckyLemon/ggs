leader_traits = {
	#Government Positions
	Head_of_Government = {
		random = no
	}

	#Ideologies
	IDEOLOGY_D = {
		random = no
		despotism_drift = 0.01
	}
	IDEOLOGY_L = {
		random = no
		liberalism_drift = 0.01
	}
	IDEOLOGY_C = {
		random = no
		conservatism_drift = 0.01
	}
	IDEOLOGY_SD = {
		random = no
		social_democratic_drift = 0.01
	}
	IDEOLOGY_S = {
		random = no
		socialism_drift = 0.01
	}
	IDEOLOGY_V = {
		random = no
		vanguardism_drift = 0.01
	}
	IDEOLOGY_NP = {
		random = no
		national_populism_drift = 0.01
	}
	IDEOLOGY_NS = {
		random = no
		national_syndicalism_drift = 0.01
	}
	IDEOLOGY_NC = {
		random = no
		national_corporatism_drift = 0.01
	}

	#Titles
	POSITION_Reichskanzler = { random = no }
	POSITION_Parteikanzler = { random = no }
	POSITION_Kaiser = { random = no }
	POSITION_Vorsitzende = { random = no }
	POSITION_Reichsvorsitzender = { random = no }
	POSITION_Prime_Minister = { random = no }
	POSITION_President = { random = no }
	POSITION_Secretary_of_General_Affairs = { random = no }
	POSITION_Commander_in_Chief = { random = no }
	POSITION_Oppositon_Leader = { random = no }

	#Vacancy Debuffs
	Vacant_HoG = {
		random = no
		political_power_gain = -0.5
		stability_factor = -0.2
		war_support_factor = -0.2
	}
	Vacant_OPP = {
		random = no
		political_power_gain = 0.2
		stability_factor = 0.1
	}
	Vacant_FM = {
		random = no
		improve_relations_maintain_cost_factor = 0.1
		trade_opinion_factor = -0.2
	}
	Vacant_DM = {
		random = no
		army_org_regain = -0.2
		army_org = -0.2
		production_speed_arms_factory_factor = -0.3
	}
	Vacant_EM = {
		random = no
		production_speed_buildings_factor = -0.3
		consumer_goods_factor = 0.3
	}
	Vacant_IM = {
		random = no
		political_power_gain = -0.2
		stability_factor = -0.2
	}
}