debug_decisions = {
	alfred_hugenberg = {
		complete_effect = {
			create_country_leader = {
				name = "Alfred Hugenberg"
				desc = "Alfred_Hugenberg_desc"
				picture = "P_D_Alfred_Hugeberg.tga"
				ideology = monarchism
				traits = { POSITION_Reichskanzler IDEOLOGY_D }
			}
		}
	}


	GER_Civil_War_Monarchist = {
		complete_effect = {
			hidden_effect = {
				every_state = {
					limit = { is_core_of = GER }
					set_core_of = BSR
					set_core_of = GWF
					set_core_of = GWR
				}
			}
			GER = {
				retire_ideology_leader = social_democratic
				retire_ideology_leader = national_populism
				retire_ideology_leader = national_syndicalism
				retire_ideology_leader = vanguardism
				set_cosmetic_tag = GER_fascist
				set_popularities = {
					despotism = 80
					national_corporatism = 20
				}
				set_politics = { ruling_party = despotism }
				create_country_leader = {
					name = "Kurt von Schleicher"
					desc = "Kurt_von_Schleicher_Desc"
					picture = "P_D_Kurt_von_Schleicher.tga"
					expire = "1965.1.1"
					ideology = dictatorialism
					traits = {
						POSITION_Commander_in_Chief IDEOLOGY_D
					}
				}
				release = AUS
			}
			AUS = {
				
			}
			BSR = {
				create_country_leader = {
					name = "Ernst Thälmann"
					desc = "Ernst_Thalmann_Desc"
					picture = "P_V_Ernst_Thalmann.tga"
					expire = "1965.1.1"
					ideology = vanguardist
					traits = {
						POSITION_Vorsitzende IDEOLOGY_V
					}
				}
				set_country_flag = BSR_bavarian_soviet_flag
				hidden_effect = {
					set_politics = { ruling_party = vanguardism }
					set_popularities = {
						socialism = 25
						vanguardism = 75
					}
				}
				transfer_state = 54
				transfer_state = 53
				transfer_state = 52
				transfer_state = 61
				transfer_state = 58
				declare_war_on = GER
			}
			#Give the communists little army support, more garrisons further down
			transfer_units_fraction = {
				target = BSR
				size = 0.4 
				stockpile_ratio = 0.1
				army_ratio = 0.1
				navy_ratio = 0
				air_ratio = 0
				keep_unit_leaders = yes
			}
			GWR = {
				create_country_leader = {
					name = "Otto Wels"
					desc = "Otto_Wels_Desc"
					picture = "P_SD_Otto_Wels.tga"
					expire = "1965.1.1"
					ideology = social_democracy
					traits = {
						POSITION_Reichskanzler IDEOLOGY_SD
					}
				}
				set_country_flag = GWR_weimar_government_flag
				hidden_effect = {
					set_politics = { ruling_party = social_democratic }
					set_popularities = {
						social_democratic = 70
						liberalism = 20
						conservatism = 10
					}
				}
				transfer_state = 65
				transfer_state = 60
				transfer_state = 59
				transfer_state = 8
				transfer_state = 50
				transfer_state = 798
				transfer_state = 804
				set_capital = 60
				declare_war_on = GER
				declare_war_on = BSR
			}
			GRR = {
				create_country_leader = {
					name = "Otto Strasser"
					desc = "Otto_Strasser_desc"
					picture = "P_NS_Otto_Strasser.tga"
					ideology = natsynd
					traits = { POSITION_Vorsitzende IDEOLOGY_NS economic_reformer syncretic_revanchist }
				}
				set_country_flag = GRR_rhein_republic_flag
				hidden_effect = {
					set_politics = { ruling_party = national_syndicalism }
					set_popularities = {
						national_syndicalism = 100
					}
				}
				transfer_state = 808
				transfer_state = 28
				transfer_state = 807
				transfer_state = 42
				transfer_state = 51
				transfer_state = 797
				transfer_state = 55
			}
			GWF = {
				create_country_leader = {
					name = "Anton Drexler"
					desc = "Anton_Drexler_desc"
					picture = "P_NP_Anton_Drexlar.tga"
					expire = "1965.1.1"
					ideology = natpop
					traits = { POSITION_Parteikanzler IDEOLOGY_NP disdain_for_politicians_1 nationalist_symbol }
				}
				set_country_flag = GWF_german_workers_flag
				hidden_effect = {
					set_politics = { ruling_party = national_populism }
					set_popularities = {
						national_populism = 100
					}
				}
				transfer_state = 803
				transfer_state = 57
				transfer_state = 56
				create_faction = "German Worker's Front"
				add_to_faction = GRR
				declare_war_on = GER
				declare_war_on = BSR
				declare_war_on = GWR
			}
		}
	}


	### Coalition Debug ###

	clear_coalitions = {
		complete_effect = {
			clear_coalitions = yes
		}
	}
	lib_coalition_change = {
		complete_effect = {
			liberal_coaltion_change = yes
		}
	}
	con_coalition_change = {
		complete_effect = {
			conservative_coaltion_change = yes
		}
	}
	socdem_coalition_change = {
		complete_effect = {
			socdem_coalition_change = yes
		}
	}
	soc_coalition_change = {
		complete_effect = {
			soc_coalition_change = yes
		}
	}
	com_coalition_change = {
		complete_effect = {
			com_coalition_change = yes
		}
	}
	despot_coalition_change = {
		complete_effect = {
			despot_coalition_change = yes
		}
	}
	natpop_coalition_change = {
		complete_effect = {
			natpop_coalition_change = yes
		}
	}
	natsynd_coalition_change = {
		complete_effect = {
			natsynd_coalition_change = yes
		}
	}
	natcorp_coalition_change = {
		complete_effect = {
			natcorp_coalition_change = yes
		}
	}
}