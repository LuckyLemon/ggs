BSR = {
	original_tag = GER
	has_country_flag = BSR_bavarian_soviet_flag
}
GWF = {
	original_tag = GER
	has_country_flag = GWF_german_workers_flag
}
GWR = {
	original_tag = GER
	has_country_flag = GWR_weimar_government_flag
}
GRR = {
	original_tag = GER
	has_country_flag = GRR_rhein_republic_flag
}