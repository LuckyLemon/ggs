bookmarks = {
	bookmark = {
		name = "GATHERING_STORM_NAME"
		desc = "GATHERING_STORM_DESC"
		date = 1930.1.1.12
		picture = "GFX_select_date_1936"
		default_country = "GER"
		default = yes
		
		"FRA"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = liberalism
			ideas = {
			}
			focuses = {
			}
		}
		"USA"={
			history = "USA_GATHERING_STORM_DESC"
			ideology = liberalism
			ideas = {
			}
			focuses = {
			}
		}
		"ENG"={
			history = "ENG_GATHERING_STORM_DESC"
			ideology = liberalism
			ideas = {
			}
			focuses = {
			}
		}
		"GER"={
			history = "GER_GATHERING_STORM_DESC"
			ideology = despotism
			ideas = {
			}
			focuses = {
			}
		}
		"ITA"={
			history = "ITA_GATHERING_STORM_DESC"
			ideology = national_corporatism
			ideas = {
			}	
			focuses = {
			}		
		}
		"JAP"={
			history = "JAP_GATHERING_STORM_DESC"
			ideology = national_corporatism
			ideas = {
			}	
			focuses = {
			}			
		}
		"SOV"={
			history = "SOV_GATHERING_STORM_DESC"
			ideology = vanguardism
			ideas = {
			}
			focuses = {
			}	
		}

		"---"={
			history = "OTHER_GATHERING_STORM_DESC"
		}


		#### minors ####

		"POL"={
			minor = yes
			history = "POL_GATHERING_STORM_DESC"
			ideology = despotism
			ideas = {
				
			}
			focuses = {
			}
		}

		"CZE"={
			minor = yes
			history = "CZE_GATHERING_STORM_DESC"
			ideology = liberalism
			ideas = {
			}
			focuses = {
			}
		}

		"HUN"={
			minor = yes
			history = "HUN_GATHERING_STORM_DESC"
			ideology = despotism
			ideas = {
			}
			focuses = {
			}
		}

		"CHI"={
			minor = yes
			history = "CHI_GATHERING_STORM_DESC"
			ideology = despotism
			ideas = {
			}
			focuses = {
			}
		}

		"MAN"={
			minor = yes
			history = "MAN_GATHERING_STORM_DESC"
			ideology = national_populism
			ideas = {
			}
			focuses = {
			}
		}
		
		effect = {
			randomize_weather = 22345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}
}
